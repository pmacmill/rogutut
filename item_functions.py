
import tcod

from game_messages import Message
from components.ai import ConfusedMonster


def heal(*args, **kwargs):
    entity = args[0]
    amount = kwargs.get('amount')

    results = []

    if entity.fighter.hp == entity.fighter.max_hp:
        results.append({'consumed': False, 'message':
                       Message('You are already at full health', tcod.yellow)})
    else:
        entity.fighter.heal(amount)
        results.append({'consumed': True, 'message':
                       Message('Your wounds start to feel better!',
                           tcod.green)})

    return results

def cast_lightning(*args, **kwargs):
    caster = args[0]
    entities = kwargs.get('entities')
    fov_map = kwargs.get('fov_map')
    damage = kwargs.get('damage')
    max_range = kwargs.get('maximum_range')

    results = []

    target = None
    closest_dist = max_range + 1

    for ent in entities:
        if ent.fighter and ent != caster and tcod.map_is_in_fov(fov_map,
                ent.x, ent.y):
            dist = caster.distance_to(ent)

            if dist < closest_dist:
                target = ent
                closest_dist = dist

    if target:
        results.append({'consumed': True, 'target': target, 'message':
                       Message('A lightning bolt strikes the {0} with a loud '
                           'thunder! The damage is {1}'.format(target.name,
                            damage))})
        results.extend(target.fighter.take_damage(damage))

    else:
        resutls.append({'consumed': False, 'target': None, 'message':
                       Message('No enemy is in range.', tcod.red)})

    return results

def cast_fireball(*args, **kwargs):
    caster = args[0]
    entities = kwargs.get('entities')
    fov_map = kwargs.get('fov_map')
    damage = kwargs.get('damage')
    radius = kwargs.get('radius')
    target_x = kwargs.get('target_x')
    target_y = kwargs.get('target_y')

    results = []

    if not tcod.map_is_in_fov(fov_map, target_x, target_y):
        results.append({'consumed': False, 'message':
                       Message('You cannot target a tile outside your '
                           'field of view.', tcod.yellow)})
        return results

    results.append({'consumed': True, 'message': Message('The fireball '
                   'explodes, burning everything within {0} tiles!'.format(
                       radius), tcod.orange)})

    for ent in entities:
        if ent.fighter and ent.distance(target_x, target_y) <= radius:
            results.append({'message': Message('The {0} gets burned for {1} '
                            'hp'.format(ent.name, damage), tcod.orange)})
            results.extend(ent.fighter.take_damage(damage))

    return results


def cast_confuse(*args, **kwargs):
    entities = kwargs.get('entities')
    fov_map = kwargs.get('fov_map')
    target_x = kwargs.get('target_x')
    target_y = kwargs.get('target_y')

    results = []

    if not tcod.map_is_in_fov(fov_map, target_x, target_y):
        results.append({'consumed': False, 'message':
                       Message('You cannot target a tile outside your '
                           'field of view.', tcod.yellow)})
        return results

    for ent in entities:
        if ent.x == target_x and ent.y == target_y and ent.ai:
            confused_ai = ConfusedMonster(ent.ai, 10)
            confused_ai.owner = ent
            ent.ai = confused_ai

            results.append({'consumed': True, 'message': Message(
                            'The eyes of the {0} look vacant, as he starts '
                            'to stumble around!'.format(ent.name),
                            tcod.light_green)})
            break
    else:
        results.append({'consumed': False, 'message': Message(
                        'There is no targetable enemy at that location',
                        tcod.yellow)})

    return results

