
import tcod

from enum import Enum

from game_states import GameStates
from menus import inventory_menu

class RenderOrder(Enum):
    STAIRS = 1
    CORPSE = 2
    ITEM = 3
    ACTOR = 4

def get_names_under_mouse(mouse, entities, fov_map):
    (x, y) = (mouse.cx, mouse.cy)

    names = [entity.name for entity in entities
             if entity.x == x and entity.y == y and
             tcod.map_is_in_fov(fov_map, entity.x, entity.y)]
    names = ', '.join(names)

    return names.capitalize()


def render_bar(panel, x, y, total_width, name, value, maximum, bar_color,
               back_color):

    bar_width = int(float(value) / maximum * total_width)

    tcod.console_set_default_background(panel, back_color)
    tcod.console_rect(panel, x, y, total_width, 1, False, tcod.BKGND_SCREEN)

    tcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        tcod.console_rect(panel, x, y, bar_width, 1, False, tcod.BKGND_SCREEN)

    tcod.console_set_default_background(panel, tcod.white)
    tcod.console_print_ex(panel, int(x + total_width/2), y, tcod.BKGND_NONE,
                          tcod.CENTER, '{0}: {1}/{2}'.format(name, value,
                          maximum))


def render_all(con, panel, entities, player, game_map, fov_map, fov_recompute,
               message_log, screen_width, screen_height, bar_width,
               panel_height, panel_y, mouse, colors, game_state):

    # Draw all the tiles in the game map
    if fov_recompute:
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = tcod.map_is_in_fov(fov_map, x, y)
                tile = game_map.tiles[x][y]
                wall = tile.block_sight

                tile_clr = None
                if visible:
                    if wall:
                        tile_clr = colors.get('light_wall')
                    else:
                        tile_clr = colors.get('light_ground')

                    tile.explored = True

                elif tile.explored:
                    if wall:
                        tile_clr = colors.get('dark_wall')
                    else:
                        tile_clr = colors.get('dark_ground')

                if tile_clr is not None:
                    tcod.console_set_char_background(con, x, y, tile_clr,
                                                 tcod.BKGND_SET)

    # Draw all entities in the list in render order
    ent_sorted = sorted(entities, key=lambda x: x.render_order.value)
    for ent in ent_sorted:
        draw_entity(con, ent, fov_map, game_map)

    tcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)

    tcod.console_set_default_background(panel, tcod.black)
    tcod.console_clear(panel)

    # Print the game messages, one line at a time
    y = 1
    for message in message_log.messages:
        tcod.console_set_default_background(panel, message.color)
        tcod.console_print_ex(panel, message_log.x, y, tcod.BKGND_NONE,
                              tcod.LEFT, message.text)
        y += 1

    render_bar(panel, 1, 1, bar_width, 'HP', player.fighter.hp,
               player.fighter.max_hp, tcod.light_red, tcod.darker_red)

    tcod.console_set_default_foreground(panel, tcod.light_gray)
    tcod.console_print_ex(panel, 1, 0, tcod.BKGND_NONE, tcod.LEFT,
                          get_names_under_mouse(mouse, entities, fov_map))

    tcod.console_blit(panel, 0, 0, screen_width, panel_height, 0, 0, panel_y)

    if game_state == GameStates.SHOW_INVENTORY:
        inventory_menu(con, 'Press the key next to an item to use it, or '
                       'Esc to cancel.\n', player.inventory, 50,
                       screen_width, screen_height)

    if game_state == GameStates.DROP_INVENTORY:
        inventory_menu(con, 'Press the key next to an item to drop it, or '
                       'Esc to cancel.\n', player.inventory, 50,
                       screen_width, screen_height)


def clear_all(con, entities):
    for ent in entities:
        clear_entity(con, ent)

def draw_entity(con, ent, fov_map, game_map):
    if tcod.map_is_in_fov(fov_map, ent.x, ent.y) or (ent.stairs and
       game_map.tiles[ent.x][ent.y].explored): 
        tcod.console_set_default_foreground(con, ent.color)
        tcod.console_put_char(con, ent.x, ent.y, ent.char, tcod.BKGND_NONE)

def clear_entity(con, ent):
    # erase the character
    tcod.console_put_char(con, ent.x, ent.y, ' ', tcod.BKGND_NONE)

